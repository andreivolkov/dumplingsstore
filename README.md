# Dumplings Store #2 (Pelmeshki Store)

Project by Andrei Volkov.

## Table of contents

[TOC]

## Links

- Deployed app: http://pelmeshki.~redacted~.ru
- Deployed app (test environment): http://pelmeshki-test.~redacted~.ru
- Gitlab: https://gitlab.~redacted~.ru/~redacted~/pelmeshki
- Grafana: http://grafana-pelmeshki.~redacted~.ru/

## Repository structure

| Derectory | Description |
|------------|----------|
| `backend` | Backend source code (GO) |
| `\Dockerfile` | Backend docker container configuration |
| `frontend` | Frontend source code (Vue Js) |
| `\Dockerfile` | Frontend docker container configuration |
| `helm-charts` | Backend & frontend helm charts |
| `yc-terraform` | Kubernetes cluster configuration (Terraform) |
| `misq` | Miscellaneous scripts |
| `\kubeconfig` | Create static authentication configuration for the CI/CD |
| `\monitoring` | Set up monitoring & logging tools |
| `.gitlab-ci.yml` | Primary CI/CD pipeline (gitlab) |
| `.gitlab-ci.backend.yml` | Backend CI/CD pipeline (gitlab) |
| `.gitlab-ci.frontend.yml` | Frontend CI/CD pipeline (gitlab) |

## Application

### Versioning
SemVer 2 is used. Major and minor versions are set manually (via `.gitlab-ci.yml`, variable `APP_VERSION`), a patch version is automatically set to an id of a top level parent pipeline. The resulting version is set to the application, containers & charts created via the CI/CD pipeline.

### Git
A Feature Branch Workflow variant is used:
- Main branch: `main`. Development branches are merged with it;
- Release branch: `live`. Only main and patch branches are allowed to be merged with it.

### Environments
Two deployment environments are used:
- Release (gitlab: `live`, kubernetes namespace: `pelmeshki`);
- Test (gitlab: `test`, kubernetes namespace: `pelmeshki-test`).
Commits to the `live` branch are automatically deployed to the release environment, for other branches the deployment to the test environment may be triggered manually via respective pipelines.

Links:
- Gitlab environments: https://gitlab.~redacted~.ru/pelmeshki/-/environments
- Deployed application (live): http://pelmeshki.~redacted~.ru
- Deployed application (test): http://pelmeshki-test.~redacted~.ru

### Docker container registry (Gitlab)
Docker containers are stored in the Gitlab container registry, versions are set:
- https://gitlab.~redacted~.ru/pelmeshki/container_registry

The latest containers are tagged as `latest` for convenience, but this tag is not used in CI/CD.

### Helm package registry (Gitlab)
Helm packages are stored in the Gitlab package registry, versions are set:
- https://gitlab.~redacted~.ru/pelmeshki/-/packages

### Secrets
All secrets used in the pipelines are stored in the Gitlab variables:
| Secret | Description |
|------------|----------|
| `GITLAB_REG_USER`,<br> `GITLAB_REG_TOKEN` | Gitlab registries username & token |
| `KUBECONFIG_YC2`,<br> `KUBECONFIG_YC2_LIVE` | Kubernetes cluster credentials files (test & release) |
| `PELMESHKI_IP`,<br> `PELMESHKI_URL`,<br> `PELMESHKI_URL_LIVE` | Nginx ingress controller IP & domain names (test & release) |
| `TLS_CERT`,<br> `TLS_KEY` | TLS certificate & key |
| `SONAR_*` | Sonarqube SAST server credentials |

### Pipeline
#### Primary pipeline (.gitlab-ci.yml)
Checks for code changes (backend & frontend) and runs the respective child pipelines (`.gitlab-ci.backend.yml`, `.gitlab-ci.frontend.yml`).

In case of the release branch (`live`) the pipeline is launched automatically, otherwise it should be run manually if needed.

Application major & minor versions are also set up here (variable `APP_VERSION`).

#### Test stage (`test`)
Various code tests are done during this stage: Gitlab SAST, Sonarqube SAST and unit tests (backend only).

Links:
- Sonarqube (backend): https://sonarqube.~redacted~.ru/dashboard?id=14_AndreiVolkov_pelmeshki_backend
- Sonarqube (frontend): https://sonarqube.~redacted~.ru/dashboard?id=14_AndreiVolkov_pelmeshki_frontend

#### Build stage (`build`)
Application components are build and packed into Docker containers. The build process is set up via the respective Docker configuration files:
- Backend (GO): `backend\Dockerfile`
- Frontend (Vue Js): `frontend\Dockerfile`

Multi-stage build process is implemented for the optimal container sizes. The ready-made containers are versioned and uploaded into the container registry:
- https://gitlab.~redacted~.ru/pelmeshki/container_registry

#### Pack stage (`pack`)
Helm packages are created for the containers built during the previous stage. Key chart values are parameterized (`values.yaml`).
- Backend: `helm-charts\charts\pelmeshki-back`
- Frontend: `helm-charts\charts\pelmeshki-front`

The ready-made packages are versioned and uploaded into the package registry:
- https://gitlab.~redacted~.ru/pelmeshki/-/packages

The chart directory also contains a parent chart which may be useful for manual deployments, it is not used in the CI/CD pipeline however.

#### Deployment stage (deploy)
Application components are deployed into a kubernetes cluster via the Helm packages.

Frontend deployment job additionally checks for the Nginx Ingress controller and TLS certificates and sets them up when required.
The Balancer is configured with a reserved IP address (variable `PELMESHKI_IP`).

Separate deploy jobs are used for the release (`live`) and test (`test`) environments, named `deploy-*` and `deploy-*-live` respectively.

## Infrastructure

### Kubernetes cluster (YC)
Application is deployed to a managed Kubernetes cluster in Yandex Cloud. The cluster should be set up (or modified) via the Terraform configuration files in the `yc-terraform` directory.

To apply Terraform configuration:

    terraform apply

Key cluster values are set in a `yc-terraform\variables.tf` file. By default, a cluster is set up with the 2 worker nodes, a static IP address is reserved for the Nginx Ingress controller balancer. All resources created via this configuration are marked as managed by Terraform.

Terraform state is stored in a private S3 bucket, versioning should be turned on.

### Additional configuration

#### Creating Kubernetes cluster namespaces

The CI/CD pipeline uses 2 namespaces, for the release and test environment respectively:

    kubectl create namespace pelmeshki
    kubectl create namespace pelmeshki-test

#### Creating static Kubernetes credential for CI/CD
1. Run the PowerShell script from the directory `misq\kubeconfig`;
2. Write down the namespace into the created configuration file (either `pelmeshki` or `pelmeshki-test`, depending on the target environment);
3. Change `certificate-authority: ca.pem` to `certificate-authority-data: <contents of file ca.pem.txt>` (file `ca.pem.txt` is also created by this script);
4. Put the final configuration file into the respective Gitlab variables (test: `KUBECONFIG_YC2`, release: `KUBECONFIG_YC2_LIVE`).

#### Setting up logging & monitoring apps (Grafana, Prometheus, Trickster)
Run the PowerShell script from the directory `misq\monitoring`. Check the documentation of the respective apps for any additional configuration needs.

Links:
- Grafana: http://grafana-pelmeshki.~redacted~.ru/
    - Guest access:
        - User: `~redacted~`
        - Pass: `~redacted~`

#### Terraform state S3 bucket
The bucket should be created manually (private, versioning is on), its credentials should be written down into the `yc-terraform\provider.tf` file.
- Bucket: https://storage.yandexcloud.net/~redacted~/

#### Images S3 bucket
The bucket for images & other media resources should be created manually (public access on, public listing off).
Resource links are set up in the Backend code:
- `backend\cmd\api\dependencies\store.go`
- `backend\cmd\api\app\app_test.go`
- Bucket: https://storage.yandexcloud.net/~redacted~/

#### Domain names, TLS certificates
Domain names and TLS certificates should be created manually via any of the service providers, the credentials set up in the Gitlab variables (more info in the Secrets paragraph).
