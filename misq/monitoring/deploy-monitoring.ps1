# Deploy a set of monitoring apps: Grafana, Prometheus & Trickster
# Ref: https://cloud.yandex.ru/docs/managed-kubernetes/tutorials/prometheus-grafana-monitoring

kubectl create namespace monitoring

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install my-prom prometheus-community/prometheus --namespace=monitoring

helm repo add tricksterproxy https://helm.tricksterproxy.io
helm repo update
helm install trickster tricksterproxy/trickster -f trickster.yaml --namespace monitoring

kubectl apply -f grafana.yaml --namespace=monitoring
kubectl apply -f grafana-ingress.yaml --namespace=monitoring
