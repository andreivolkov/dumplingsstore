# Create a static kuber/yc config for the ci/cd
# Ref: https://cloud.yandex.ru/docs/managed-kubernetes/operations/connect/create-static-conf

$CLUSTER_ID = "<CLUSTER_ID>"
$CLUSTER = yc managed-kubernetes cluster get --id $CLUSTER_ID --format json | ConvertFrom-Json
$CLUSTER.master.master_auth.cluster_ca_certificate | Set-Content ca.pem
kubectl create -f sa.yaml
$SECRET = kubectl -n kube-system get secret -o json | `
  ConvertFrom-Json | `
  Select-Object -ExpandProperty items | `
  Where-Object { $_.metadata.name -like "*admin-user*" }
$SA_TOKEN = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($SECRET.data.token))
$MASTER_ENDPOINT = $CLUSTER.master.endpoints.external_v4_endpoint
kubectl config set-cluster pelmeshki-k8s `
  --certificate-authority=ca.pem `
  --server=$MASTER_ENDPOINT `
  --kubeconfig=pelmeshki.kubeconfig
kubectl config set-credentials admin-user `
  --token=$SA_TOKEN `
  --kubeconfig=pelmeshki.kubeconfig
kubectl config set-context pelmeshki `
  --cluster=pelmeshki-k8s `
  --user=admin-user `
  --kubeconfig=pelmeshki.kubeconfig
kubectl config use-context pelmeshki `
  --kubeconfig=pelmeshki.kubeconfig
certutil -f -encode "ca.pem" "ca.pem.txt"