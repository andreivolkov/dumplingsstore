# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.95.0"
  constraints = ">= 0.87.0"
  hashes = [
    "h1:A0U767cW75U+oZtIhf61PlmKxMESbQtEX5ngfPo9MRk=",
  ]
}
