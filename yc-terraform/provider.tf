terraform {
  required_version = ">= 0.13"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.87"
    }
  }

  # Store Terraform state to s3 bucket
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "pelmeshki-back"
    region     = "ru-central1"
    key        = "yc-terraform/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}
