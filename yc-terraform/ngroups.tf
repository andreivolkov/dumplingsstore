# K8S node groups

resource "yandex_kubernetes_node_group" "myngroup" {
  name        = var.ngroup_name
  description = var.pelmeshki_description
  cluster_id  = yandex_kubernetes_cluster.mykuber.id
  version     = var.k8s_version

  instance_template {
    platform_id = var.ngroup_platform_id

    network_interface {
      nat                = var.ngroup_nat
      subnet_ids         = [yandex_vpc_subnet.mysubnet.id]
    }

    resources {
      cores         = var.ngroup_res_cores
      core_fraction = var.ngroup_res_coref
      memory        = var.ngroup_res_memory
    }

    boot_disk {
      type = var.ngroup_disk_type
      size = var.ngroup_disk_size
    }

    scheduling_policy {
      preemptible = var.ngroup_scheduling_policy
    }

    container_runtime {
      type = var.ngroup_container_runtime
    }
  }

  scale_policy {
    fixed_scale {
      size = var.ngroup_scale_policy_size
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "sunday"
      start_time = "01:00"
      duration   = "5h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "01:00"
      duration   = "5h"
    }
  }
}