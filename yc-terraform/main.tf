# Pelmeshki Store

# K8S cluster (managed)
resource "yandex_kubernetes_cluster" "mykuber" {
  name        = var.k8s_name
  description = var.pelmeshki_description
  
  network_id = yandex_vpc_network.mynet.id
  
  master {
    version = var.k8s_version
    
    zonal {
      zone      = yandex_vpc_subnet.mysubnet.zone
      subnet_id = yandex_vpc_subnet.mysubnet.id
    }

    public_ip = var.k8s_public_ip

    security_group_ids = [
      yandex_vpc_security_group.mykuber-public-services.id,
      yandex_vpc_security_group.mykuber-master-whitelist.id
    ]
  }
  
  service_account_id      = yandex_iam_service_account.myaccount.id
  node_service_account_id = yandex_iam_service_account.myaccount.id
  depends_on = [
    yandex_resourcemanager_folder_iam_member.k8s-clusters-agent,
    yandex_resourcemanager_folder_iam_member.vpc-public-admin,
    yandex_resourcemanager_folder_iam_member.images-puller,
    yandex_resourcemanager_folder_iam_member.editor
  ]

  kms_provider {
    key_id = yandex_kms_symmetric_key.mykms-key.id
  }
}

# Network
resource "yandex_vpc_network" "mynet" {
  name        = var.vpc_network_name
  description = var.pelmeshki_description
  folder_id   = var.folder_id
}

# Subnet
resource "yandex_vpc_subnet" "mysubnet" {
  description = var.pelmeshki_description
  v4_cidr_blocks = [var.ip_range]
  zone           = var.zone
  network_id     = yandex_vpc_network.mynet.id
}

# Encryption key
resource "yandex_kms_symmetric_key" "mykms-key" {
  name              = var.kms_name
  description       = var.pelmeshki_description
  default_algorithm = var.kms_algorithm
  rotation_period   = var.kms_rotation
}

# Reserved static IP address for the nginx-ingress
resource "yandex_vpc_address" "myip" {
  name                = var.reserved_ip_name
  description         = var.pelmeshki_description
  folder_id           = var.folder_id
  deletion_protection = true
  
  external_ipv4_address {
    zone_id = var.zone
  }
}