variable "cloud_id" {
  description   = "Cloud ID"
  type          = string
  default       = "b1g98mnpapdi7qjecmti"
}

variable "folder_id" {
  description   = "Folder ID"
  type          = string
  default       = "b1g0c6fjir4eh943mo8e"
}

variable "zone" {
  description   = "Instance availability zone"
  type          = string
  default       = "ru-central1-a"
}

variable "sa_name" {
  description   = "Service account name"
  type          = string
  default       = "sa-terraform1"
}

variable "pelmeshki_description" {
  description   = "Cloud console description for the Pelmeshki Store resources managed by this Terraform config"
  type          = string
  default       = "(Managed by Terraform) Pelmeshki Store resource."
}

variable "k8s_version" {
  description   = "Kubernetes cluster version"
  type          = string
  default       = "1.24"
}

variable "k8s_name" {
  description   = "Kubernetes cluster name"
  type          = string
  default       = "pelmeshki-k8s"
}

variable "k8s_public_ip" {
  description   = "Kubernetes cluster API public ip"
  type          = bool
  default       = true
}

variable "ip_range" {
  description   = "Subnet IP range"
  type          = string
  default       = "10.1.0.0/16"
}

variable "kms_name" {
  description   = "KMS symmetric key name"
  type          = string
  default       = "pelmeshki-kms-key"
}

variable "kms_rotation" {
  description   = "KMS symmetric key rotation period"
  type          = string
  default       = "8760h" # 1 year
}

variable "kms_algorithm" {
  description   = "KMS symmetric key encryption algorithm"
  type          = string
  default       = "AES_128"
}

variable "k8s_api_allowed_ips" {
  description = "External IPs whitelisted for k8s api access"
  default     = (["0.0.0.0/0"]) # ["94.140.209.0/24"]
  type        = set(string)
}

variable "ngroup_name" {
  description   = "Node group name"
  type          = string
  default       = "pelmeshki-ngroup1"
}

variable "ngroup_platform_id" {
  description   = "Node group platform id"
  type          = string
  default       = "standard-v3" # https://cloud.yandex.ru/docs/compute/concepts/vm-platforms
}

variable "ngroup_nat" {
  description   = "Node group NAT"
  type          = bool
  default       = true
}

variable "ngroup_res_cores" {
  description   = "Node group resources - CPU cores"
  type          = number
  default       = 2
}

variable "ngroup_res_coref" {
  description   = "Node group resources - CPU cores fraction (%)"
  type          = number
  default       = 20
}

variable "ngroup_res_memory" {
  description   = "Node group resources - Memory (Gb)"
  type          = number
  default       = 4
}

variable "ngroup_disk_type" {
  description   = "Node group boot disk type"
  type          = string
  default       = "network-hdd" # https://cloud.yandex.com/en-ru/docs/compute/concepts/disk
}

variable "ngroup_disk_size" {
  description   = "Node group boot disk size (Gb)"
  type          = number
  default       = 64
}

variable "ngroup_scheduling_policy" {
  description   = "Node group scheduling policy - is preemptible"
  type          = bool
  default       = false
}

variable "ngroup_container_runtime" {
  description   = "Node group container runtime - Docker or containerd"
  type          = string
  default       = "containerd" # https://cloud.yandex.ru/docs/managed-kubernetes/concepts/#config
}

variable "ngroup_scale_policy_size" {
  description   = "Node group instance number (currently configured as fixed)"
  type          = number
  default       = 2
}

variable "reserved_ip_name" {
  description   = "Reserved static IP name"
  type          = string
  default       = "pelmeshki-ip1"
}

variable "vpc_network_name" {
  description   = "VPC network name"
  type          = string
  default       = "pelmeshki-net"
}
