# Service account for cluster management
resource "yandex_iam_service_account" "myaccount" {
  name        = var.sa_name
  description = var.pelmeshki_description
}

# Service account - adding role "k8s.clusters.agent"
resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}

# Service account - adding role "vpc.publicAdmin"
resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}

# Service account - adding role "container-registry.images.puller"
resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}

# Service account - adding role "editor"
resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}

# Service account - adding role "kms.viewer"
resource "yandex_resourcemanager_folder_iam_member" "viewer" {
  folder_id = var.folder_id
  role      = "kms.viewer"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}
