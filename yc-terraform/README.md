## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (>= 0.13)

- <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) (>= 0.87)

## Providers

The following providers are used by this module:

- <a name="provider_yandex"></a> [yandex](#provider\_yandex) (0.95.0)

## Modules

No modules.

## Resources

The following resources are used by this module:

- [yandex_iam_service_account.myaccount](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/iam_service_account) (resource)
- [yandex_kms_symmetric_key.mykms-key](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kms_symmetric_key) (resource)
- [yandex_kubernetes_cluster.mykuber](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_cluster) (resource)
- [yandex_kubernetes_node_group.myngroup](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_node_group) (resource)
- [yandex_resourcemanager_folder_iam_member.editor](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) (resource)
- [yandex_resourcemanager_folder_iam_member.images-puller](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) (resource)
- [yandex_resourcemanager_folder_iam_member.k8s-clusters-agent](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) (resource)
- [yandex_resourcemanager_folder_iam_member.viewer](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) (resource)
- [yandex_resourcemanager_folder_iam_member.vpc-public-admin](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) (resource)
- [yandex_vpc_address.myip](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_address) (resource)
- [yandex_vpc_network.mynet](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_network) (resource)
- [yandex_vpc_security_group.mykuber-master-whitelist](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_security_group) (resource)
- [yandex_vpc_security_group.mykuber-public-services](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_security_group) (resource)
- [yandex_vpc_subnet.mysubnet](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/vpc_subnet) (resource)

## Required Inputs

No required inputs.

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_cloud_id"></a> [cloud\_id](#input\_cloud\_id)

Description: Cloud ID

Type: `string`

Default: `"b1g98mnpapdi7qjecmti"`

### <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id)

Description: Folder ID

Type: `string`

Default: `"b1g0c6fjir4eh943mo8e"`

### <a name="input_ip_range"></a> [ip\_range](#input\_ip\_range)

Description: Subnet IP range

Type: `string`

Default: `"10.1.0.0/16"`

### <a name="input_k8s_api_allowed_ips"></a> [k8s\_api\_allowed\_ips](#input\_k8s\_api\_allowed\_ips)

Description: External IPs whitelisted for k8s api access

Type: `set(string)`

Default:

```json
[
  "0.0.0.0/0"
]
```

### <a name="input_k8s_name"></a> [k8s\_name](#input\_k8s\_name)

Description: Kubernetes cluster name

Type: `string`

Default: `"pelmeshki-k8s"`

### <a name="input_k8s_public_ip"></a> [k8s\_public\_ip](#input\_k8s\_public\_ip)

Description: Kubernetes cluster API public ip

Type: `bool`

Default: `true`

### <a name="input_k8s_version"></a> [k8s\_version](#input\_k8s\_version)

Description: Kubernetes cluster version

Type: `string`

Default: `"1.24"`

### <a name="input_kms_algorithm"></a> [kms\_algorithm](#input\_kms\_algorithm)

Description: KMS symmetric key encryption algorithm

Type: `string`

Default: `"AES_128"`

### <a name="input_kms_name"></a> [kms\_name](#input\_kms\_name)

Description: KMS symmetric key name

Type: `string`

Default: `"pelmeshki-kms-key"`

### <a name="input_kms_rotation"></a> [kms\_rotation](#input\_kms\_rotation)

Description: KMS symmetric key rotation period

Type: `string`

Default: `"8760h"`

### <a name="input_ngroup_container_runtime"></a> [ngroup\_container\_runtime](#input\_ngroup\_container\_runtime)

Description: Node group container runtime - Docker or containerd

Type: `string`

Default: `"containerd"`

### <a name="input_ngroup_disk_size"></a> [ngroup\_disk\_size](#input\_ngroup\_disk\_size)

Description: Node group boot disk size (Gb)

Type: `number`

Default: `64`

### <a name="input_ngroup_disk_type"></a> [ngroup\_disk\_type](#input\_ngroup\_disk\_type)

Description: Node group boot disk type

Type: `string`

Default: `"network-hdd"`

### <a name="input_ngroup_name"></a> [ngroup\_name](#input\_ngroup\_name)

Description: Node group name

Type: `string`

Default: `"pelmeshki-ngroup1"`

### <a name="input_ngroup_nat"></a> [ngroup\_nat](#input\_ngroup\_nat)

Description: Node group NAT

Type: `bool`

Default: `true`

### <a name="input_ngroup_platform_id"></a> [ngroup\_platform\_id](#input\_ngroup\_platform\_id)

Description: Node group platform id

Type: `string`

Default: `"standard-v3"`

### <a name="input_ngroup_res_coref"></a> [ngroup\_res\_coref](#input\_ngroup\_res\_coref)

Description: Node group resources - CPU cores fraction (%)

Type: `number`

Default: `20`

### <a name="input_ngroup_res_cores"></a> [ngroup\_res\_cores](#input\_ngroup\_res\_cores)

Description: Node group resources - CPU cores

Type: `number`

Default: `2`

### <a name="input_ngroup_res_memory"></a> [ngroup\_res\_memory](#input\_ngroup\_res\_memory)

Description: Node group resources - Memory (Gb)

Type: `number`

Default: `4`

### <a name="input_ngroup_scale_policy_size"></a> [ngroup\_scale\_policy\_size](#input\_ngroup\_scale\_policy\_size)

Description: Node group instance number (currently configured as fixed)

Type: `number`

Default: `2`

### <a name="input_ngroup_scheduling_policy"></a> [ngroup\_scheduling\_policy](#input\_ngroup\_scheduling\_policy)

Description: Node group scheduling policy - is preemptible

Type: `bool`

Default: `false`

### <a name="input_pelmeshki_description"></a> [pelmeshki\_description](#input\_pelmeshki\_description)

Description: Cloud console description for the Pelmeshki Store resources managed by this Terraform config

Type: `string`

Default: `"(Managed by Terraform) Pelmeshki Store resource."`

### <a name="input_reserved_ip_name"></a> [reserved\_ip\_name](#input\_reserved\_ip\_name)

Description: Reserved static IP name

Type: `string`

Default: `"pelmeshki-ip1"`

### <a name="input_sa_name"></a> [sa\_name](#input\_sa\_name)

Description: Service account name

Type: `string`

Default: `"sa-terraform1"`

### <a name="input_vpc_network_name"></a> [vpc\_network\_name](#input\_vpc\_network\_name)

Description: VPC network name

Type: `string`

Default: `"pelmeshki-net"`

### <a name="input_zone"></a> [zone](#input\_zone)

Description: Instance availability zone

Type: `string`

Default: `"ru-central1-a"`

## Outputs

No outputs.
